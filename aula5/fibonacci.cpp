/*
A serie de Fibonacci � formada pela sequ�ncia: 
1, 1, 2, 3, 5, 8, 13, 21, 34, 55, ...
Escrever um programa em Linguagem C que receba um valor inteiro.
Seu programa deve conter uma fun��o que recebe no par�metro 
um n�mero que indica a posi��o do termo e retorna o valor 
correspondente na sequ�ncia de Fibonacci.
*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>

int fibonacci(int termo){
	int a1 = 1;
	int a2 = 1;
	int k = 2;
	int prox;
	
	if(termo==1 || termo==2){
		return 1;
	}
	
	while(k<termo){
		prox = a1 + a2;
		a1 = a2;
		a2 = prox;
		k++;
	}
	return prox;
}

main(){
	//ajustando o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	int ter;
	
	//entrada de dados
	printf("Digite um n�mero: ");
	scanf("%d", &ter);
	
	printf("O %d� termo da s�rie de Fibinacci � %d\n", ter, fibonacci(ter));
	
	getch();
}





