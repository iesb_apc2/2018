/*Escrever um programa para calcular a pot�ncia de um n�mero. 
Seu programa deve conter uma fun��o recursiva que receba como 
par�metros o n�mero e o expoente e a fun��o retorna a pot�ncia.*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>

int potencia(int base, int exp){
	if(exp != 0){
		return base * potencia(base, --exp);
	}
	else{
		return 1;
	}
}

main(){
	//ajustar idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	int b, e;
	
	//entrada
	printf("Digite a base e o expoente: ");
	scanf("%d %d", &b, &e);
	
	//sa�da
	printf("%d elevado a %d � %d\n\n", b, e, potencia(b, e));
	
	getch();
}



