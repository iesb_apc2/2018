/*Escrever uma fun��o recusiva para calcular o mdc entre 
dois n�meros*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>

int mdc(int n1, int n2){
	if(n2!=0){
		return mdc(n2, n1%n2);
	}
	else{
		return n1;
	}
}

main(){
	//ajustar idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	int num1, num2;
	
	//entrada
	printf("Digite os n�meros: ");
	scanf("%d %d", &num1, &num2);
	
	//sa�da
	printf("O MCD entre %d e %d � %d\n\n", num1, num2, mdc(num1, num2));
	
	getch();
}

