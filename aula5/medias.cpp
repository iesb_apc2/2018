/*
Escrever um programa em linguagem C para receber tr�s notas de um aluno.
Seu programa deve conter uma fun��o que receba como par�metro as tr�s notas e mais um letra:
Caso a letra for 'A' a fun��o retorna a m�dia aritm�tica
Caso a letra for 'P' a fun��o retorna a m�dia ponderada, 
com os pesos com pesos 5, 3 e 2
*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
float media(float n1, float n2, float n3, char tipo){
	if(tipo=='A'){
		return (n1 + n2 + n3) / 3;
	}
	else if(tipo=='P'){
		return (n1*5 + n2*3 + n3*2) / 10;
	}
}

main(){
	//ajustar idioma
	setlocale(LC_ALL, "Portuguese");
	//declara��es
	float n1, n2, n3;
	
	//entrada de dados
	printf("Digite as 3 notas: ");
	scanf("%f %f %f", &n1, &n2, &n3);
	
	//sa�da
	printf("A m�dia aritm�tica � %0.2f\n\n", media(n1, n2, n3, 'A'));
	
	printf("A m�dia ponderada � %0.2f\n\n", media(n1, n2, n3, 'P'));
	
	getch();
}
