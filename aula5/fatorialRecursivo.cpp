/*Escrever uma fun��o recursiva para calcular o fatorial 
de um n�mero.*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>

int fatorial(int n){
	if(n!=1){
		return n * fatorial(n-1);
	}
	else{
		return 1;
	}
}

int somaRec(int n){
	if(n!=1){
		return n + somaRec(n-1);
	}
	else{
		return 1;
	}
}

main(){
	//ajustar idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	int num;
	
	//entrada
	printf("Digite um n�mero: ");
	scanf("%d", &num);
	
	//sa�da
	printf("O fatorial de %d � %d\n\n", num, fatorial(num));
	printf("Soma recursiva de %d � %d\n\n", num, somaRec(num));
	
	getch();
}
