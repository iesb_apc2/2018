/*
STRUCTS
Escrever um programa para modelar a ficha de funcion�rios
da empresa FACA-ME RIR Com�rcio e Representa��es.
A ficha do funcion�rio � descrita conforme o seguinte:
	char matricula[10]
	char nome[30]
	int dependentes
	float salario
Monte um vetor com 5 fichas de funcion�rio e imprima o custo
mensal com o pagamento destes funcion�rios.
*/


#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h> //biblioteca de idiomas
#define N 5
//definindo um novo tipo chamado funcionario
typedef struct funcionario FUNCIONARIO;
//descrevendo como � o novo tipo funcionario
struct funcionario{
	char matricula[10];
	char nome[30];
	int dependentes;
	float salario;
};

main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	FUNCIONARIO func[N];
	int k;
	float custo=0;
	
	//entrada de dados
	for(k=0; k<N; k++){
		fflush(stdin);  //limpa o buffer do teclado
		printf("Digite a matr�cula do funcion�rio: ");
		gets(func[k].matricula);
		
		fflush(stdin);
		printf("Digite o nome do funcion�rio: ");
		gets(func[k].nome);
		
		printf("Digite a quantidade de dependentes: ");
		scanf("%d", &func[k].dependentes);
		
		printf("Digite o salario do funcion�rio: ");
		scanf("%f", &func[k].salario);
		
		system("cls");
	}
	
	//system("cls"); //limpa a tela
	
	for(k=0; k<N; k++){
		printf("Matr�cula: %s\n", func[k].matricula);
		printf("Nome: %s\n", func[k].nome);
		printf("Dependentes: %d\n", func[k].dependentes);
		printf("Sal�rio: %0.2f\n", func[k].salario);
		printf("-----------------------------\n");
		
		custo = custo + func[k].salario;  //acumulando os sal�rios dos funcion�rios		
	}
	
	printf("\nCusto mensal com sal�rios: %0.2f\n", custo);
	getch();
}


