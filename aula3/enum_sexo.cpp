#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<string.h>

typedef struct sexo SEXO;
struct sexo{
	char desc[10];
};

enum {
	MASCULINO,
	FEMININO
};



main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declarações
	int sx;
	
	//inicicializando o vetor de sexos
	SEXO sexos[2];
	strcpy(sexos[0].desc, "homem");
	strcpy(sexos[1].desc, "mulher");
	
	
	printf("Digite o sexo (0-homem /1-mulher): ");
	scanf("%d", &sx);
	
	printf("%s\n", sexos[sx].desc);
	
	getch();
}
