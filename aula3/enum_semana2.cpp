/*
Escrever um programa em linguagem C para enumerar os
dias da semana*/

/*
Alterar o programa de enumera��o da semana para permitir
que uma lanchonete fa�a promo��o geral com descontos de 30% 
nos dias de semana. Final de semana o pre�o � normal.
*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<string.h>

typedef struct diaSemana DIASEMANA;
struct diaSemana{
	char desc[20];
};

enum semana{
	DOMINGO,
	SEGUNDA,
	TERCA,
	QUARTA,
	QUINTA,
	SEXTA,
	SABADO
};

main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	DIASEMANA sema[7];
	strcpy(sema[0].desc, "Domingo");
	strcpy(sema[1].desc, "Segunda-feira");
	strcpy(sema[2].desc, "Ter�a-feira");
	strcpy(sema[3].desc, "Quarta-feira");
	strcpy(sema[4].desc, "Quinta-feira");
	strcpy(sema[5].desc, "Sexta-feira");
	strcpy(sema[6].desc, "S�bado");
	
	int dia;	
	/*printf("Domingo = %d\n", DOMINGO);
	printf("Quarta = %d\n", QUARTA);*/
	
	printf("Digite o dia: ");
	scanf("%d", &dia);

	switch(dia){
		case SEGUNDA:
		case TERCA:
		case QUARTA:
		case QUINTA:
		case SEXTA:
			printf("Hoje � dia: %s\n", sema[dia].desc);	
			printf("Lanchonete com desconto de 30 porcento");
		break;
		case SABADO:
		case DOMINGO:
			printf("Hoje � dia: %s\n", sema[dia].desc);	
			printf("Pre�o normal no fim de semana!");
		break;
		default:
			printf("Dia inv�lido!");
		break;
	}
	
	getch();
}
