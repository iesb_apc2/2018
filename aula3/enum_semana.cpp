/*
Escrever um programa em linguagem C para enumerar os
dias da semana*/

/*
Alterar o programa de enumera��o da semana para permitir
que uma lanchonete fa�a promo��o geral com descontos de 30% 
nos dias de semana. Final de semana o pre�o � normal.
*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	enum semana{
		DOMINGO = 1,
		SEGUNDA,
		TERCA,
		QUARTA,
		QUINTA,
		SEXTA,
		SABADO
	};
	
	int dia;	
	/*printf("Domingo = %d\n", DOMINGO);
	printf("Quarta = %d\n", QUARTA);*/
	
	printf("Digite o dia: ");
	scanf("%d", &dia);
	
	switch(dia){
		case SEGUNDA:
		case TERCA:
		case QUARTA:
		case QUINTA:
		case SEXTA:
			printf("Lanchonete com desconto de 30 %");
		break;
		case SABADO:
		case DOMINGO:
			printf("Pre�o normal no fim de semana!");
		break;
		default:
			printf("Dia inv�lido!");
		break;
	}
	
	getch();
}
