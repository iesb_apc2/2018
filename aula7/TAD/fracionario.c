/*fracionario.c  --> arquivo de implementa��es*/
#include<string.h>
#include "fracionario.h"

int getNum(fracao a){  //retorna o numerador da fra��o a
	return a.num;
}

int getDen(fracao a){  //retorna o denominador da fra��o a
	return a.den;
}

fracao criar(int numerador, int denominador){
	fracao f;
	f.num = numerador;
	f.den = denominador;
	return f;
}

fracao somar(fracao a, fracao b){
	fracao result;
	result.num = (a.num * b.den) + (b.num * a.den);
	result.den = (a.den * b.den);
	return result;
}

fracao subtrair(fracao a, fracao b){
	fracao result;
	result.num = (a.num * b.den) - (b.num * a.den);
	result.den = (a.den * b.den);
	return result;
}

fracao multiplicar(fracao a, fracao b){
	fracao result;
	result.num = (a.num * b.num);
	result.den = (a.den * b.den);
	return result;
}

fracao dividir(fracao a, fracao b){
	fracao result;
	result.num = (a.num * b.den);
	result.den = (a.den * b.num);
	return result;
}


char* mostrar(fracao a){
	static char s[30];
	sprintf(s, "%d / %d", getNum(a), getDen(a));
	return(s);
}

fracao simplificar(fracao a){
	int x, y, z;
	if(a.num > a.den){  //testar quem � o maior entre numerador e denominador
		x = a.num;
		y = a.den;
	}
	else{
		x = a.den;
		y = a.num;
	}
	z = mdc(x,y);
	
	fracao result;
	result.num = a.num / z;
	result.den = a.den / z;
	
	return result;
}

int mdc(int a, int b){
	if(b!=0){
		return mdc(b, (a%b));
	}
	return a;
}
