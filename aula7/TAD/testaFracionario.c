#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <locale.h>
#include "fracionario.h"

main() {
	setlocale(LC_ALL, "Portuguese");
	fracao f1 = criar(2, 3);
	fracao f2 = criar(4, 5);
	fracao f2simp = simplificar(f2);
	
	fracao f3 = criar(16, 64);
	fracao f3simp = simplificar(f3);
	
	fracao total = somar(f1, f2);
	fracao diferenca = subtrair(f1, f2);
	fracao produto = multiplicar(f1, f2);
	fracao quociente = dividir(f1, f2);
	fracao quocienteSimp = simplificar(quociente);
	
	printf("f1 = %s\n", mostrar(f1));
	printf("f2 = %s\n", mostrar(f2));
	printf("f2 simplificada = %s\n\n", mostrar(f2simp));
	printf("f3 = %s\n", mostrar(f3));
	printf("f3 simplificada = %s\n\n", mostrar(f3simp));
	
	
	
	printf("Soma = %s\n", mostrar(total));
	printf("Subtra��o = %s\n", mostrar(diferenca));
	printf("Multiplica��o = %s\n", mostrar(produto));
	printf("Divis�o = %s\n", mostrar(quociente));
	printf("Divis�o simplificada = %s\n", mostrar(quocienteSimp));
	
	getch();
}


/*
Escrever uma fun��o para simplificar uma fra��o dada.
A fun��o dever� ter o seguinte prot�tipo:

fracao simplificar(fracao a);

*/
