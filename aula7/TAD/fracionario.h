/*fracionario.h -> Arquivo de prot�tipos de fun��es*/
typedef struct est{
	int num;  //numerador
	int den;  //denominador
}fracao;

int getNum(fracao a);  //retorna o numerador da fra��o a

int getDen(fracao a);  //retorna o denominador da fra��o a

fracao criar(int numerador, int denominador);

fracao somar(fracao a, fracao b);

fracao subtrair(fracao a, fracao b);

fracao multiplicar(fracao a, fracao b);

fracao dividir(fracao a, fracao b);

char* mostrar(fracao a);

fracao simplificar(fracao a);

int mdc(int a, int b);
