/*
EXERC�CIO 1
Escrever um programa para alimentar um vetor de inteiros de 10 posi��es.
Seu programa deve imprimir:
	* a soma dos elementos do vetor
	* a soma dos elementos das posi��es pares do vetor
	* a soma dos elementos das posi��es �mpares do vetor
	* o maior valor armazenado
	* o menor valor armazenado
*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
main(){
	setlocale(LC_ALL, "Portuguese"); //ajustando o idioma
	int numero[10];
	int k, soma=0, pares=0, impares=0, max, min; 

	//alimentando o vetor	
	for(k=0; k<10; k++){
		//entrada de dados
		printf("Digite um n�mero: ");
		scanf("%d", &numero[k]);
		soma = soma + numero[k];  //acumulando a soma geral dos n�meros do vetor
		
		if(k % 2 == 0){  //a posi��o � par
			pares = pares + numero[k];
		}
		else{ //� �mpar
			impares = impares + numero[k];
		}
		
		//inicializando m�ximo e m�nimo
		if(k==0){  //primeira itera��o do la�o
			max = numero[k];
			min = numero[k];
		}
		
		//para as pr�ximas itera��es
		if(numero[k] > max){
			max = numero[k];
		}
		if(numero[k] < min){
			min = numero[k];
		}
	}
	
	system("cls"); //limpa a tela
	printf("Posi��es do vetor:\n");
	for(k=0; k<10; k++){
		printf("Posi��o %d = %d\n", k, numero[k]);
	}
	
	printf("\n\n----------------------\n");
	printf("Soma dos elementos do vetor: %d\n", soma);
	printf("Soma dos elementos das posi��es pares do vetor: %d\n", pares);	
	printf("Soma dos elementos das posi��es �mpares do vetor: %d\n", impares);
	printf("Valor m�ximo armazenado no vetor: %d\n", max);
	printf("Valor m�nimo armazenado no vetor: %d\n", min);
	
	getch();
}


