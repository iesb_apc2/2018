/*
EXERC�CIO 1
Escrever um programa para alimentar um vetor de inteiros de 10 posi��es.
Seu programa deve imprimir:
	* a soma dos elementos do vetor
	* a soma dos elementos das posi��es pares do vetor
	* a soma dos elementos das posi��es �mpares do vetor
	* o maior valor armazenado
	* o menor valor armazenado
*/
/*
EXERCICIO 2
Escrever um programa para alimentar uma matriz 5x5 de inteiros
Seu programa deve imprimir:
	* a soma dos elementos da matriz
	* a soma dos elementos das linhas pares
	* a soma dos elementos das linhas �mpares
	* a soma dos elementos da diagonal principal
	* a soma dos elementos da diagonal secund�ria
*/

/*
A secretaria de seguran�a do DF resolveu cadastrar os Blocos do Carnaval de rua de Bras�lia.
Escreva um programa que alimente um vetor de 5 blocos de carnaval. 
Utilize um struct que representa o bloco conforme o seguinte:
	bloco   	tipo caractere com 30 posi��es
	integrantes	tipo inteiro

Seu programa deve imprimir:
	uma lista de todos os blocos;
	o bloco com mais integrantes;
	o total de integrantes de todos os blocos de carnaval do DF
*/

