/*
EXERCICIO 2
Escrever um programa para alimentar uma matriz 5x5 de inteiros
Seu programa deve imprimir:
	* a soma dos elementos da matriz
	* a soma dos elementos das linhas pares
	* a soma dos elementos das linhas �mpares
	* a soma dos elementos da diagonal principal
	* a soma dos elementos da diagonal secunc�ria
*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
#define N 5
main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	int matrix[N][N];
	int lin, col, soma=0, pares=0, impares=0, somaDP=0, somaDS=0;
	
	//alimentando a matriz
	for(lin=0; lin<N; lin++){
		for(col=0; col<N; col++){
			printf("Digite o valor da posi��o (%d , %d): ", lin, col);
			scanf("%d", &matrix[lin][col]);
		}
	}
	
	system("cls");
	//imprimindo a matriz
	printf("\n\n--------------------------\n");
	for(lin=0; lin<N; lin++){  //navegando nas linhas
		for(col=0; col<N; col++){  //navegando nas colunas
			printf("\t %d ", matrix[lin][col]);
			
			soma = soma + matrix[lin][col];
			if(lin%2==0){  //linha par
				pares = pares + matrix[lin][col];
			}
			else{  //linha �mpar
				impares  = impares + matrix[lin][col];
			}
			
			//diagonal principal
			/*if(lin==col){
				somaDP = somaDP + matrix[lin][col];
			}*/
			
			
			//diagonal secund�ria
			if(lin+col == N-1){
				somaDS = somaDS + matrix[lin][col];
			}
		}
		somaDP = somaDP + matrix[lin][lin];
		printf("\n");
	}
	
	printf("\n\n--------------------------\n");
	printf("Soma dos elementos: %d\n", soma);
	printf("Soma das linhas pares: %d\n", pares);
	printf("Soma das linhas �mpares: %d\n", impares);
	printf("Soma da diagonal principal: %d\n", somaDP);
	printf("Soma da diagonal secund�ria: %d\n", somaDS);
	
	
	getch();
}

