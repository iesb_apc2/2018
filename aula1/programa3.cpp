/*
A secretaria de seguran�a do DF resolveu cadastrar os Blocos do Carnaval de rua 
de Bras�lia.
Escreva um programa que alimente um vetor de 5 blocos de carnaval. 
Utilize um struct que representa o bloco conforme o seguinte:
	nome   	tipo caractere com 30 posi��es
	integrantes	tipo inteiro

Seu programa deve imprimir:
	uma lista de todos os blocos;
	o bloco com mais integrantes;
	o total de integrantes de todos os blocos de carnaval do DF
*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
#define N 5

typedef struct bloco BLOCO;
struct bloco{
	char	nome[30];
	int 	integrantes;
};

main(){
	//ajustando o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	BLOCO carna2018[N];
	int k, total=0;
	
	BLOCO maior;
	
	
	//estrutura de repeti��o para alimentar o vetor carna2018
	for(k=0; k<N; k++){
		fflush(stdin);
		printf("Digite o nome do bloco: ");
		gets(carna2018[k].nome);
		
		printf("Digite o n�mero de integrantes do bloco: ");
		scanf("%d", &carna2018[k].integrantes);
		
		system("cls");
	}
	
	//inicializando o bloco com mais integrantes
	maior = carna2018[0];
	//estrutura de repeti��o para imprimir a lista de blocos do vetor carna2018
	for(k=0; k<N; k++){
		printf("Bloco: %s\n", carna2018[k].nome);
		printf("Integrantes: %d\n\n", carna2018[k].integrantes);

		//calcular as informa��es pedidas
		total = total + carna2018[k].integrantes;
		
		if(carna2018[k].integrantes > maior.integrantes){
			maior = carna2018[k];
		}
	}
	
	printf("\n--------------------------------\n");
	printf("Total de foli�es no carnaval de Bras�lia: %d\n", total);
	printf("Maior bloco: %s\n", maior.nome);
	
	getch();
	
}
