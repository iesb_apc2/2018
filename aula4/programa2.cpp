#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>

int soma(int a, int b){
	int s = a + b;
	return s;
}

int subtracao(int a, int b){
	int d = a - b;
	return d;
}

int multiplicacao(int a, int b){
	int m = a * b;
	return m;
}

float divisao(int a, int b){
	float q = a / (b * 1.0);
	return q;
}

main(){
	setlocale(LC_ALL, "Portuguese");
	
	int n1, n2;
	
	printf("Digite dois n�meros inteiros: ");
	scanf("%d %d", &n1, &n2);
	
	printf("A soma �: %d\n", soma(n1, n2));
	printf("A diferen�a �: %d\n", subtracao(n1, n2));
	printf("O produto �: %d\n", multiplicacao(n1, n2));
	printf("O quociente �: %0.2f\n", divisao(n1, n2));
	
	
	getch();
}
