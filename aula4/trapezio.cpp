/*
Escreva um programa em linguagem C que receba tr�s valores 
flutuantes que representam a altura, a base maior e a base 
menor de um trap�zio ret�ngulo.
Seu programa deve conter 
	* uma fun��o para calcular a �rea do trap�zio
	* uma fun��o para calcular o per�metro do trap�zio.
*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include "funcoes.h"

main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	float b1, b2, h;
	
	//entrada de dados
	printf("Digite base maior, base menor e altura: ");
	scanf("%f %f %f", &b1, &b2, &h);
	
	//sa�da
	printf("�rea = %0.2f\n", areaTrapezioRet(b1, b2, h));
	printf("Per�metro = %0.2f\n", perimetroTrapezioRet(b1, b2, h));

	getch();	
}

