#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>

//declara��o de prot�tipo de fun��o
void cabecalho();  
void linha();

main(){
	setlocale(LC_ALL, "Portuguese");
	
	cabecalho(); 
	
	getch();
}

//implementa��o de fun��es
void cabecalho(){
	linha();
	printf("Nome: Fulano de Tal\n");
	printf("Matr�cula: 889964542\n");	
	linha();	
}

void linha(){
	printf("============================\n");
}
