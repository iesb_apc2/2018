//funcoes.c
//implementa��es
//implementa��es de fun��es
float areaCirculo(float raio){
	//return M_PI * raio * raio;
	return M_PI * pow(raio, 2);
}

float comprimentoCirculo(float raio){
	return 2 * M_PI * raio;
}

//implementa��es do trap�zio
float areaTrapezioRet(float base1, float base2, float alt){
	return ((base1 + base2) * alt)/2;
}

float perimetroTrapezioRet(float base1, float base2, float alt){
	float x;
	x = sqrt( pow((base1-base2), 2) + pow(alt, 2));
	
	return base1 + base2 + alt + x;
}


//fatorial
int fatorial(int n){
	int k;
	int fat=1;
	for(k=n; k>0; k--){
		fat = fat * k;
	}
	return fat;
}
