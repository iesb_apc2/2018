/*
Escrever um programa em linguagem C que receba um n�mero
Seu programa deve conter uma fun��o para calcular o fatorial 
do n�mero recebido
*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include "funcoes.h"

main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	int num;
	
	//entrada
	printf("Digite um n�mero: ");
	scanf("%d", &num);
	
	//sa�da
	printf("Fatorial = %d\n", fatorial(num));
	
	getch();
}
