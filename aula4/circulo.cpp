/*
Escreva um programa que receba o raio de um c�rculo.
Em seu programa, crie:
	* uma fun��o que retorne a �rea do c�rculo
	* uma fun��o que retorne a comprimento da circunfer�ncia.
	
	�rea do c�rculo: pi * raio�
	comprimento da circunfer�ncia: 2 * pi * raio
*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>

//prot�tipos
float area(float);
float comprimento(float);

main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	float raio;
	
	//entrada
	printf("Digite o raio: ");
	scanf("%f", &raio);
	
	
	//sa�da
	printf("�rea = %0.2f\n", area(raio));
	printf("Comprimento = %0.2f\n", comprimento(raio));
	getch();
}


//implementa��es de fun��es
float area(float raio){
	//return M_PI * raio * raio;
	return M_PI * pow(raio, 2);
}

float comprimento(float raio){
	return 2 * M_PI * raio;
}

