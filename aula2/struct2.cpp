/*
Escrever um programa para alimentar um struct de produto 
conforme o seguinte:

nome do produto (caractere 20 posi��es)
pre�o do produto (flutuante)
quantidade no estoque (inteiro)
*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>

typedef struct produto PRODUTO;
struct produto{
	char nome[20];
	float preco;
	int qtd;
};

main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	PRODUTO prod;
	
	//entrada de dados
	fflush(stdin);
	printf("Nome do produto: ");
	gets(prod.nome);
	
	printf("Digite o pre�o: ");
	scanf("%f", &prod.preco);
	
	printf("Digite a quantidade em estoque: ");
	scanf("%d", &prod.qtd);
	
	//sa�da
	system("cls");
	printf("Nome: %s\n", prod.nome);
	printf("Pre�o: %0.2f\n", prod.preco);
	printf("Quantidade: %d\n", prod.qtd);
	
	getch();
}
