/*
Escrever um programa para alimentar um struct de produto 
conforme o seguinte:

nome do produto (caractere 20 posi��es)
pre�o do produto (flutuante)
quantidade no estoque (inteiro)
*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
#define N 3
typedef struct produto PRODUTO;
struct produto{
	char nome[20];
	float preco;
	int qtd;
};

main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	PRODUTO estoque[N];
	int k;
	
	//entrada de dados
	for(k=0; k<N; k++){
		fflush(stdin);
		printf("Nome do produto: ");
		gets(estoque[k].nome);
		
		printf("Digite o pre�o: ");
		scanf("%f", &estoque[k].preco);
		
		printf("Digite a quantidade em estoque: ");
		scanf("%d", &estoque[k].qtd);
	}
	//sa�da
	system("cls");
	for(k=0; k<N; k++){
		printf("Nome: %s\n", estoque[k].nome);
		printf("Pre�o: %0.2f\n", estoque[k].preco);
		printf("Quantidade: %d\n", estoque[k].qtd);
		printf("\n-----------------------------------------\n");
	}
	getch();
}
