#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>


typedef float (*processa) (float, float);

//montando a fun��o gen�rica
float area(float x, float y, processa calcular){
	calcular(x, y);
}

float areaTriangulo(float base, float alt){
	return (base * alt) / 2;
}

float areaRetangulo(float lado1, float lado2){
	return lado1 * lado2;
}

main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	float a, b;
	
	//entrada de dados
	printf("Digite a base e a altura do tri�ngulo: ");
	scanf("%f %f", &a, &b);
	
	printf("�rea do tri�ngulo %0.2f\n\n", area(a, b, &areaTriangulo));


	printf("\n\nDigite os lados do ret�ngulo: ");
	scanf("%f %f", &a, &b);
	
	printf("�rea do ret�ngulo %0.2f\n\n", area(a, b, &areaRetangulo));
	
	getch();
}
