#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
//sintaxe tipo_retornado_pelas_fun��es (*ponteiro) (lista_de_tipos)
//exemplo: int (*ptr) (int, int)

typedef int (*compara)(int, int);

int resultado(int a, int b, compara comp){
	return (comp(a, b));
}

int min(int a, int b){
	return (a < b) ? a : b;
}

int max(int a, int b){
	return (a > b) ? a : b;
}

main(){
	int x = 10;
	int y = 20;

	printf("Menor = %d\n", resultado(x, y, &min));
	printf("Maior = %d\n", resultado(x, y, &max));

	//printf("Menor = %d\n", min(x, y));
	//printf("Maior = %d\n", max(x, y));

	
	getch();
}
