#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
/*
      * --> conte�do de 
      & --> endere�o de
*/

void troca(int *a, int *b){ //passagem de par�metros por REFER�NCIA
	int aux;
	aux = *a;
	*a = *b;
	*b = aux;
}

int soma(int a, int b){ //passagem de par�metros por VALOR
	return a + b;
}

main(){
	setlocale(LC_ALL, "Portuguese");
	int x = 10;
	int y = 20;
	int aux;
	
	printf("Imprimindo os conte�dos ANTES da troca\n");
	printf("x = %d\n", x);
	printf("y = %d\n", y);

	//trocando os conte�dos
	troca(&x, &y);

	printf("\n\nImprimindo os conte�dos AP�S a troca\n");
	printf("x = %d\n", x);
	printf("y = %d\n", y);
	
	printf("soma = %d\n", soma(x, y));
	
	getch();
}
