#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
#include<math.h>
//sintaxe de ponteiro para fun��o
// tipo_retornado (*ponteiro) (lista de tipos)

//fun��o gen�rica
/*float operacao(float a, float b, float (*calcula)(float, float)){
	return calcula(a, b);
}*/

typedef float (*calcula)(float, float);

float operacao(float a, float b, calcula calc){
	return calc(a, b);
}


float soma(float a, float b){
	return a + b;
}

float subtracao(float a, float b){
	return a - b;
}

float multiplicacao(float a, float b){
	return a * b;
}

float divisao(float a, float b){
	return a / b;
}

float potencia(float b, float e){
	return pow(b, e);
}

main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//declara��es
	float x, y;
	
	//entrada de dados
	printf("Digite os n�meros: ");
	scanf("%f %f", &x, &y);
	
	printf("Soma = %0.2f\n", operacao(x, y, &soma));
	printf("Subra��o = %0.2f\n", operacao(x, y, &subtracao));
	printf("Produto = %0.2f\n", operacao(x, y, &multiplicacao));
	printf("Quociente = %0.2f\n", operacao(x, y, &divisao));
	printf("Pot�ncia = %0.2f\n", operacao(x, y, &potencia));
	
	getch();
}
