#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
#define N 3
main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	int** tabela = (int**) malloc(N * sizeof(int*));
	int lin, col;
	
	if(tabela == NULL){
		printf("Erro: Mem�ria insuficiente!\n");
	}
	else{
		for(lin=0; lin<N; lin++){
			tabela[lin] = (int*) malloc(N * sizeof(int));
			for(col=0; col<N; col++){
				printf("Digite tabela[%d][%d]: ", lin, col);
				scanf("%d", &tabela[lin][col]);
			}
		}
		
		for(lin=0; lin<N; lin++){
			for(col=0; col<N; col++){
				printf("\t %d", tabela[lin][col]);
			}
			printf("\n");
		}
		
		free(tabela);
	}
	
	getch();
}
