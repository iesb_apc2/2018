/*Exerc�cio 4
Criar um struct para armazenar as informa��es de um evento esportivo conforme o seguinte
	evento
	valor do ingresso
	assento
Seu programa deve imprimir o valor total arrecadado com o evento.
Liberar mem�ria ao final do programa.*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
#define N 5
typedef struct ingresso INGRESSO;
struct ingresso{
	char evento[10];
	float valor;
	int assento;
};

main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	INGRESSO* lista = (INGRESSO*) malloc(N * sizeof(INGRESSO));
	float arrecadacao = 0;
	
	if(lista == NULL){
		printf("Erro: Mem�ria Insuficiente!!\n");
	}
	else{
		//entrada de dados
		int k;
		for(k=0; k<N; k++){
			printf("\n\n--------------------------\n");
			printf("Ingresso %d\n", k);
			fflush(stdin);
			printf("Evento: ");
			gets(lista[k].evento);
			
			printf("Valor: ");
			scanf("%f", &lista[k].valor);
			
			printf("Assento: ");
			scanf("%d", &lista[k].assento);
		}
		
		//limpar a tela
		system("cls");
		for(k=0; k<N; k++){
			printf("Evento: %s\n", lista[k].evento);
			printf("Valor: %0.2f\n", lista[k].valor);
			printf("Assento: %d\n", lista[k].assento);
			printf("-----------------------\n\n");
			
			arrecadacao += lista[k].valor;
		}
		
		printf("\n\nTotal arrecadado = %0.2f\n", arrecadacao);
		
		free(lista);
	}
	
	getch();
}
