/*Exerc�cio 3
Altere o programa do produto para que imprima o valor do estoque de cada produto
(quantidade * pre�o do produto)
Liberar mem�ria ao final do programa.*/
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>

typedef struct produto PRODUTO;

struct produto{
	char  nome[20];
	int   qtd;
	float preco;
};


main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//alocando 5 posi��es de mem�ria para armazenar 5 produtos
	PRODUTO* lista = (PRODUTO*) malloc(5 * sizeof(PRODUTO)); 
	
	if(lista == NULL){
		printf("Mem�ria Insuficiente!\n");
	}
	else{
		//entrada de dados
		int k;
		for(k=0; k<5; k++){
			printf("Produto %d\n", k);
			fflush(stdin);
			printf("Digite o nome: ");
			gets(lista[k].nome);
			
			printf("Digite a quantidade: ");
			scanf("%d", &lista[k].qtd);
			
			printf("Digite o pre�o: ");
			scanf("%f", &lista[k].preco);
		}
		
		system("cls");
		float valorEstoque = 0;
		for(k=0; k<5; k++){
			valorEstoque = lista[k].qtd * lista[k].preco;
			printf("Nome: %s\n", lista[k].nome);	
			printf("Quantidade: %d\n", lista[k].qtd);
			printf("Pre�o: %0.2f\n", lista[k].preco);
			printf("Valor do estoque = %0.2f\n", valorEstoque);
			printf("----------------------------------------\n\n");
		}
	}
	
	free(lista);
	
	getch();
}
