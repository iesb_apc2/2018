#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>

main(){
	setlocale(LC_ALL, "Portuguese");
	printf("Tamanho que um tipo inteiro ocupa em mem�ria: ");
	printf("%d\n", sizeof(int));
	
	//int *val = (int*) malloc(200);
	int *val = (int*) malloc(50 * sizeof(int));
	if(*val == NULL){
		printf("Sem mem�ria dispon�vel");
	}
	else{
		printf("Aloquei 50 posi��es de inteiros que � igual a 200 bytes ");
		printf("(50 * %d = 200 bytes)\n\n\n", sizeof(int));
	}
	
	
	getch();
}
