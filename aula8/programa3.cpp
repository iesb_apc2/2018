#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>

main(){
	setlocale(LC_ALL, "Portuguese");
	//int *val = (int*) malloc(200);
	int *val = (int*) malloc(5 * sizeof(int)); //criei um vetor de inteiros
	if(*val == NULL){
		printf("Sem mem�ria dispon�vel");
	}
	else{
		printf("Aloquei 5 posi��es de inteiros que � igual a 20  bytes ");
		printf("(5 * %d = 200 bytes)\n\n\n", sizeof(int));
		
		int k;
		for(k=0; k<5; k++)	{
			printf("Digite val[%d]: ", k);
			scanf("%d", &val[k]);
		}
		
		system("cls");
		
		for(k=0; k<5; k++)	{
			printf("%d\n", val[k]);
		}
		
		free(val);
		
		printf("\n\n");
		
		for(k=0; k<5; k++){
			printf("%d\n", val[k]);
		}
		
	}
	

	
	getch();
}
