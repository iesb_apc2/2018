/*Exerc�cio2
Criar e alimentar um vetor de valores flutuantes com aloca��o din�mica de mem�ria. 
Imprimir os valores armazenados e liberar a mem�ria
Imprimir a soma dos valores armazenados.*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<locale.h>
#define N 5
main(){
	//ajustar o idioma
	setlocale(LC_ALL, "Portuguese");
	
	//aloca��o din�mica dos valores float
	float soma = 0;
	float *valores = (float*) malloc(N * sizeof(float));
	
	if(valores == NULL) {
		printf("Erro: Mem�ria insuficiente!\n\n");
	}
	else {
		//entrada de dados
		int k;
		for(k=0; k<N; k++){
			printf("Digite o valor da posi��o %d: ", k);
			scanf("%f", &valores[k]);
		}
		
		for(k=0; k<N; k++){
			soma += valores[k];
			printf("Valor %d = %0.2f\n", k, valores[k]);
		}
		
		printf("\n\nSoma = %0.2f\n\n", soma);
		
		free(valores);
	}
	getch();
}
